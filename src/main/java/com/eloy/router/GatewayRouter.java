package com.eloy.router;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayRouter {

    @Value("${gateway.path}")
    private String baseUrl;

    @Bean
    public RouteLocator customGatewayRouter(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("generic_query_id", p -> p
                        .path("/**/search/id={id}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("generic_edit", p -> p
                        .path("/**/edit/id={id}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("literature_query_title", p -> p
                        .path("/literature/search/title={title}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("literature_query_poi", p -> p
                        .path("/literature/search/poi={poi}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("literature_query_isbn", p -> p
                        .path("/literature/search/isbn={isbn}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("literature_query_url", p -> p
                        .path("/literature/search/url={url}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("project_query_name", p -> p
                        .path("/project/search/name={name}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .build();
    }
}
