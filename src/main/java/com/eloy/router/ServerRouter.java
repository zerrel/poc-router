package com.eloy.router;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServerRouter {

    @Value("${server.path}")
    private String baseUrl;

    @Bean
    public RouteLocator customServerRouter(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("generic_query_all", p -> p
                        .path("/**/search/all")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("generic_create", p -> p
                        .path("/**/new")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("literature_query_title", p -> p
                        .path("/literature/search/title={title}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("literature_query_author", p -> p
                        .path("/literature/search/author={author}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("literature_query_datePublished", p -> p
                        .path("/literature/search/date-published={date}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .route("literature_query_peer_reviewed", p -> p
                        .path("/literature/search/peer-reviewed={peerReviewed}")
                        .filters(f -> f.setStatus(302))
                        .uri(baseUrl))
                .build();
    }
}
