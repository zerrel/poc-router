package com.eloy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocRouterApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocRouterApplication.class, args);
	}

}
